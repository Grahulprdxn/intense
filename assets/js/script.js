
$(document).ready(function() {

	// Back To Top Starts here
	var backbtn = document.querySelector(".back-to-top");

	window.onscroll = function() {

		if(document.documentElement.scrollTop > 300){
			backbtn.classList.add("back-display");
		}
		else {
			backbtn.classList.remove("back-display");
		}
	}
	
	backbtn.addEventListener("click", function() {
		event.preventDefault();
	  window.scrollTo({
	  	top: 0,
	  	behavior: 'smooth'
	  });
	});
	// Back To Top Ends here

	// Slick Slider Starts here
	$(".lazy").slick({
    dots: true,
    appendArrows: null,
    autoplay: true,
    autoplaySpeed: 1000
  });
	// Slick Slider Ends here

	// Hamburger Starts here
	$(".hamburger").click(function() {
		$(".contact-us").toggle();
		$(".book-now").toggle();
		$(".hamburger").toggleClass("active");
	});
	// Hamburger Ends here
});






















